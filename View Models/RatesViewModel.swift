//
//  RatesViewModel.swift
//  RevolutTestApp
//
//  Created by Marcin Zbijowski on 16/01/2019.
//  Copyright © 2019 Marcin Zbijowski. All rights reserved.
//

import Foundation
import UIKit

class RatesViewModel: NSObject {
    
    private let tableView: UITableView
    private let dataSource: RatesDataSource
    private var amount: Float? = 1.0
    private let numberFormatter = NumberFormatter()
    private var latestRates: [CurrencyRate] = []
    private var isChangingAmount = false
    
    init(tableView: UITableView, dataSource: RatesDataSource) {
        self.tableView = tableView;
        self.dataSource = dataSource
        
        super.init()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.setupFormatter()
        self.dataSource.delegate = self
        self.dataSource.startGettingLatestRates()
    }
    
    private func setupFormatter() {
        self.numberFormatter.locale = Locale.current
        self.numberFormatter.minimumFractionDigits = 0
        self.numberFormatter.maximumFractionDigits = 2
        self.numberFormatter.minimumIntegerDigits = 1
    }
    
    private func recalculateAmounts(rates: [CurrencyRate]) -> [CurrencyRate] {
        return rates.map {
            var el = $0
            if let amount = self.amount {
                el.calculatedAmount = amount * $0.rate
            } else {
                el.calculatedAmount = nil
            }
            
            return el
        }
    }
    
}

extension RatesViewModel: RatesDataSourceDelegate {
    
    func didLoadLatestRates(_ rates: [CurrencyRate], previousCount: Int) {
        if (self.isChangingAmount) {
            return
        }

        self.latestRates = self.recalculateAmounts(rates: rates)
        
        if (previousCount > 1) {
            self.reloadRows(from: 1, to: previousCount-1)
        } else {
            self.tableView.reloadData()
        }
    }
    
    func reloadRows(from: Int, to: Int) {
        let paths = (from...to).map { IndexPath(row: $0, section: 0) }
        self.tableView.reloadRows(at: paths, with: .automatic)
    }
    
}

extension RatesViewModel: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.latestRates.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InputCell", for: indexPath) as! InputCell
        let rate = self.latestRates[indexPath.row]
        cell.configure(with: rate, formatter: self.numberFormatter)
        if (indexPath.row == 0) {
            cell.delegate = self
        }
        return cell
    }

}

extension RatesViewModel: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.amount = self.latestRates[indexPath.row].calculatedAmount
        self.dataSource.updateBaseCurrency(self.latestRates[indexPath.row].currency)
        self.latestRates.swapAt(indexPath.row, 0)
        tableView.reloadRows(at: [indexPath, IndexPath(row: 0, section: 0)], with: .automatic)
    }
    
}

extension RatesViewModel: InputCellDelegate {
    
    func textFieldDidChange(text: String?) {
        if let text = text {
            self.amount = Float(text)
        } else {
            self.amount = nil
        }

        self.latestRates = self.recalculateAmounts(rates: self.latestRates)
        self.reloadRows(from: 1, to: self.latestRates.count-1)
    }
    
    func textFieldDidFocus(_ textfield: UITextField) {
        self.isChangingAmount = true
    }
    
    func textFieldDidLostFocus(_ textfield: UITextField) {
        self.isChangingAmount = false
    }
    
}
