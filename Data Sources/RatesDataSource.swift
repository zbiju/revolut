//
//  RatesDataSource.swift
//  RevolutTestApp
//
//  Created by Marcin Zbijowski on 16/01/2019.
//  Copyright © 2019 Marcin Zbijowski. All rights reserved.
//

import Foundation

protocol RatesDataSourceDelegate {
    func didLoadLatestRates(_ rates: [CurrencyRate], previousCount: Int) -> Void
}

class RatesDataSource {
    
    private let api: RatesAPI
    private var baseCurrency: Currency
    private var timer: Timer?
    private var previousCount = 0
    public var delegate: RatesDataSourceDelegate?
    
    init(api: RatesAPI) {
        self.api = api
        self.baseCurrency = self.api.currency(for: "EUR")
    }

    public func startGettingLatestRates() {
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(makeLatestCall), userInfo: nil, repeats: true)
    }
    
    public func stopGettingLatestRates() {
        self.timer?.invalidate()
        self.timer = nil
    }
    
    public func updateBaseCurrency(_ currency: Currency) {
        self.baseCurrency = currency
    }
    
    @objc private func makeLatestCall() {
        self.api.getLatestRates(for: self.baseCurrency.symbol, success: { [weak self] (rates) in
            guard let `self` = self else { return }
            
            DispatchQueue.main.async {
                let latestRates = [CurrencyRate(currency: self.baseCurrency, rate: 1)] + rates
                self.delegate?.didLoadLatestRates(latestRates, previousCount: self.previousCount)
                self.previousCount = latestRates.count
            }
        }) { (err) in
            return 
        }
    }
}
