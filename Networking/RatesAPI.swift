//
//  RatesAPI.swift
//  RevolutTestApp
//
//  Created by Marcin Zbijowski on 16/01/2019.
//  Copyright © 2019 Marcin Zbijowski. All rights reserved.
//

import Foundation

fileprivate let __latest_task_key = "LATEST"

class RatesAPI {
    
    public enum ResponseError: Error {
        case emptyData
    }
    
    private let baseUrl: String
    private let session: URLSession
    private var runningTasks: Dictionary<String, URLSessionDataTask> = [:]
    private var symbols: Dictionary<String, String>?
    
    init(baseUrl: String, session: URLSession = .shared) {
        self.baseUrl = baseUrl
        self.session = session
        
        guard let symbolsPath = Bundle.main.path(forResource: "CurrencySymbols", ofType: "plist"),
            let symbolNames = NSDictionary(contentsOfFile: symbolsPath)
            else { return }
        
        self.symbols = symbolNames as? Dictionary<String, String>
    }
    
    public func currency(for symbol: String) -> Currency {
        guard let name = self.symbols?[symbol]
        else {
            return Currency(symbol: symbol, name: nil)
        }
        
        return Currency(symbol: symbol, name: name)
    }
    
    public func getLatestRates(for base: String, success: (([CurrencyRate]) -> Void)? = nil, failure: ((Error) -> Void)? = nil) {
        guard let url = URL(string: "\(self.baseUrl)/latest?base=\(base)") else { return }
        if let latestTask = self.runningTasks[__latest_task_key] {
            latestTask.cancel()
            self.runningTasks[__latest_task_key] = nil
        }

        let task = self.session.dataTask(with: url) { (data, response, error) in
            if let error = error {
                DispatchQueue.main.async {
                    failure?(error)
                }
            }
            
            guard let data = data else {
                DispatchQueue.main.async {
                    failure?(RatesAPI.ResponseError.emptyData)
                }
                return
            }
        
            do {
                let response = try JSONDecoder().decode(RateAPIResponse.self, from: data)
                let rates = response.rates.map { [weak self] (symbol, rate) -> CurrencyRate in
                    let currency = Currency(symbol: symbol, name: self?.symbols?[symbol])
                    return CurrencyRate(currency: currency, rate: rate)
                }
                DispatchQueue.main.async {
                    success?(rates)
                }
            } catch {
                DispatchQueue.main.async {
                    failure?(error)
                }
            }
        }
        self.runningTasks[__latest_task_key] = task
        task.resume()
    }
    
}
