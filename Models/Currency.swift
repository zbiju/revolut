//
//  Currency.swift
//  RevolutTestApp
//
//  Created by Marcin Zbijowski on 16/01/2019.
//  Copyright © 2019 Marcin Zbijowski. All rights reserved.
//

import Foundation

struct Currency {
    public var symbol: String
    public var name: String?
}
