//
//  CurrencyRate.swift
//  RevolutTestApp
//
//  Created by Marcin Zbijowski on 16/01/2019.
//  Copyright © 2019 Marcin Zbijowski. All rights reserved.
//

import Foundation

struct CurrencyRate {
    public let currency: Currency
    public let rate: Float
    public var calculatedAmount: Float?
    
    init(currency: Currency, rate: Float) {
        self.currency = currency
        self.rate = rate
        self.calculatedAmount = nil
    }
}
