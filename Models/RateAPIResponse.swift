//
//  RateAPIResponse.swift
//  RevolutTestApp
//
//  Created by Marcin Zbijowski on 16/01/2019.
//  Copyright © 2019 Marcin Zbijowski. All rights reserved.
//

import Foundation

struct RateAPIResponse: Codable {
    public var base: String
    public var date: String
    public var rates: Dictionary<String, Float>
}
