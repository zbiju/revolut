//
//  RevolutTestAppTests.swift
//  RevolutTestAppTests
//
//  Created by Marcin Zbijowski on 16/01/2019.
//  Copyright © 2019 Marcin Zbijowski. All rights reserved.
//

import XCTest
@testable import RevolutTestApp

class URLSessionDataTaskMock: URLSessionDataTask {
    private let closure: () -> Void
    
    init(closure: @escaping () -> Void) {
        self.closure = closure
    }
    
    override func resume() {
        closure()
    }
}

class URLSessionMock: URLSession {
    typealias CompletionHandler = (Data?, URLResponse?, Error?) -> Void
    
    var data: Data?
    var error: Error?
    
    override func dataTask(with url: URL, completionHandler: @escaping CompletionHandler) -> URLSessionDataTask {
        let data = self.data
        let error = self.error
        
        return URLSessionDataTaskMock {
            completionHandler(data, nil, error)
        }
    }
}

class RevolutTestAppTests: XCTestCase {

    let rates = [
        CurrencyRate(currency: Currency(symbol: "ABC", name: "Fake currency 1"), rate: 1),
        CurrencyRate(currency: Currency(symbol: "XYZ", name: "Fake currency 2"), rate: 2)
    ]
    
    var viewModel: RatesViewModel!
    var tableView: UITableView!
    var ratesDataSource: RatesDataSource!

    override func setUp() {
        guard let ctrl = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateInitialViewController() as? ViewController else {
            XCTFail("Cannot instantiate main controller")
            return
        }
        _ = ctrl.view
        let session = URLSessionMock()
        session.data = "{\"base\":\"EUR\",\"date\":\"2018-09-06\",\"rates\":{\"AUD\":1.6103,\"BGN\":1.9484,\"PLN\":4.3019}}".data(using: .utf8)
        self.ratesDataSource = RatesDataSource(api: RatesAPI(baseUrl: "", session: session))
        self.viewModel = RatesViewModel(tableView: ctrl.tableView,
                                        dataSource: self.ratesDataSource)
        self.tableView = ctrl.tableView
    }

    override func tearDown() {
    }

    func testUpdatingRates() {
        XCTAssertEqual(self.viewModel.tableView(self.tableView, numberOfRowsInSection: 0), 0)
        
        self.viewModel.didLoadLatestRates(self.rates, previousCount: 0)
        XCTAssertEqual(self.viewModel.tableView(self.tableView, numberOfRowsInSection: 0), 2)
    }
    
    func testCell() {
        self.viewModel.didLoadLatestRates(self.rates, previousCount: 0)
        let cell = self.viewModel.tableView(self.tableView, cellForRowAt: IndexPath(row: 0, section: 0))
        XCTAssertTrue(cell is InputCell)
    }
    
    func testAmountChangeText() {
        self.viewModel.didLoadLatestRates(self.rates, previousCount: 0)
        let input = ["1", "123", ""]
        let expected = ["2", "246", ""]
        for i in 0..<input.count {
            self.viewModel.textFieldDidChange(text: input[i])
            
            if let cell = self.viewModel.tableView(self.tableView, cellForRowAt: IndexPath(row: 1, section: 0)) as? InputCell {
                XCTAssertEqual(cell.amountTextField.text, expected[i])
            } else {
                XCTFail("Wrong cell type")
            }
        }
    }
    
    func testCurrencyChange() {
        self.viewModel.didLoadLatestRates(self.rates, previousCount: 0)
        if let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? InputCell {
            XCTAssertEqual(cell.symbolLabel.text, "ABC")
            self.viewModel.tableView(self.tableView, didSelectRowAt: IndexPath(row: 1, section: 0))
            if let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? InputCell {
                XCTAssertEqual(cell.symbolLabel.text, "XYZ")
            } else {
                XCTFail("Wrong cell type")
            }
        } else {
            XCTFail("Wrong cell type")
        }
    }
    
    func testBaseCurrencyChange() {
        let session = URLSessionMock()
        session.data = "{\"base\":\"EUR\",\"date\":\"2018-09-06\",\"rates\":{\"AUD\":1.6103,\"BGN\":1.9484,\"PLN\":4.3019}}".data(using: .utf8)
        let api = RatesAPI(baseUrl: "", session: session)
        var updatedRates: [CurrencyRate]? = nil
        let expectation = self.expectation(description: "get latest")
        api.getLatestRates(for: "EUR", success: { (rates) in
            updatedRates = rates
            expectation.fulfill()
        })
        
        waitForExpectations(timeout: 2, handler: nil)
        XCTAssertNotNil(updatedRates)
        XCTAssertEqual(updatedRates?.count, 3)
    }

}
