//
//  ViewController.swift
//  RevolutTestApp
//
//  Created by Marcin Zbijowski on 16/01/2019.
//  Copyright © 2019 Marcin Zbijowski. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!

    var ratesViewModel: RatesViewModel?
    var api = RatesAPI(baseUrl: "https://revolut.duckdns.org")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.ratesViewModel = RatesViewModel(tableView: self.tableView, dataSource: RatesDataSource(api: self.api))
    }


}

