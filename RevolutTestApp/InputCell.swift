//
//  InputCell.swift
//  RevolutTestApp
//
//  Created by Marcin Zbijowski on 16/01/2019.
//  Copyright © 2019 Marcin Zbijowski. All rights reserved.
//

import UIKit

protocol InputCellDelegate {
    func textFieldDidChange(text: String?) -> Void
    func textFieldDidFocus(_ textfield: UITextField) -> Void
    func textFieldDidLostFocus(_ textfield: UITextField) -> Void
}

class InputCell: UITableViewCell {

    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var symbolLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    private(set) var rate: CurrencyRate?
    public var delegate: InputCellDelegate? {
        didSet {
            self.amountTextField.isUserInteractionEnabled = self.delegate != nil
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.amountTextField.text = nil
        self.symbolLabel.text = nil
        self.nameLabel.text = nil
        self.amountTextField.isUserInteractionEnabled = false
    }
    
    public func configure(with rate: CurrencyRate, formatter: NumberFormatter) {
        self.rate = rate
        self.symbolLabel.text = rate.currency.symbol
        self.nameLabel.text = rate.currency.name
        if let amount = rate.calculatedAmount {
            self.amountTextField.text = formatter.string(for: amount)
        } else {
            self.amountTextField.text = nil
        }
    }

    @IBAction func textFieldChanged(_ sender: UITextField) {
        self.delegate?.textFieldDidChange(text: sender.text)
    }
    
    @IBAction func textFieldFocused(_ sender: UITextField) {
        self.delegate?.textFieldDidFocus(sender)
    }
    
    @IBAction func textFieldLostFocus(_ sender: UITextField) {
        self.delegate?.textFieldDidLostFocus(sender)
    }

}
